module CommonLibrary
open System.Text.RegularExpressions

// +------------------------------------------------------+
// |                                                      |
// |  # Generic Operators                                 |
// |                                                      |
// +------------------------------------------------------+

let (<!>) (f : 'a -> 'b) (r : Result<'a, 'c>) : Result<'b, 'c> =
  r |> Result.map f

let (<*>) 
  (f : Result<'a -> 'b, 'c list>)
  (r : Result<'a, 'c list>) 
  : Result<'b, 'c list> =
  
  match f, r with
  | Ok f,     Ok r      -> f r      |> Ok
  | Error e,  Ok _      -> e        |> Error
  | Ok _,     Error e   -> e        |> Error
  | Error e1, Error e2  -> e1 @ e2  |> Error

let (<**>)
  (r1 : Result<'a list, 'b list>)
  (r2 : Result<'a, 'b list>)
  : Result<'a list, 'b list> =

  match r1, r2 with
  | Ok v1,    Ok v2     -> v1 @ [v2]  |> Ok
  | Error e,  Ok _      -> e          |> Error
  | Ok _,     Error e   -> e          |> Error
  | Error e1, Error e2  -> e1 @ e2    |> Error

let (<!*>)
  (r1 : Result<'a, 'b list>)
  (r2 : Result<'a, 'b list>)
  : Result<'a list, 'b list> =

  let r1' = r1 |> Result.map (fun v -> [v])
  r1' <**> r2

let (|+>) r f = Result.bind f r

let (|*>) r f = Result.map f r 

let append
  (r1 : Result<'a list, 'b list>)
  (r2 : Result<'a list, 'b list>)
  : Result<'a list, 'b list> =

  match r1, r2 with
  | Ok v1,    Ok v2     -> v1 @ v2 |> Ok
  | Ok _,     Error e   -> e |> Error
  | Error e,  Ok _      -> e |> Error
  | Error e1, Error e2  -> e1 @ e2 |> Error

let mandatory f (e : string) (dto : 'a option) =
  match dto with
  | Some v -> f v
  | None -> [e] |> Error

let toList (x : 'a) : 'a list = [x]

// +------------------------------------------------------+
// |                                                      |
// |  # Validation                                        |
// |                                                      |
// +------------------------------------------------------+

type Validation =
  | Valid
  | Invalid of string list

let maxLength (n : int) (i : string) : bool =
  i |> String.length <= n

let minLength (n : int) (i : string) : bool =
  i |> String.length >= n

let matchRegex (r : string) (i : string) : bool =
  Regex.IsMatch(i, r)

let notNegative (i : decimal) : bool =
  i >= 0m

let inClosedInterval (a : decimal) (b : decimal) (i : decimal) : bool =
  (i >= a) && (i <= b)

let valMaxLength (n : int) (i : string) : Validation =
  let l = i |> String.length
  match maxLength n i with
  | true -> Valid
  | false -> Invalid [sprintf "Maxlength is %d but was %d" n l]

let valMinLength (n : int) (i : string) : Validation =
  let l = i |> String.length
  match minLength n i with
  | true -> Valid
  | false -> Invalid [sprintf "Minlength is %d but was %d" n l]

let valNotNegative (i : decimal) : Validation =
  match notNegative i with
  | true -> Valid
  | false -> Invalid [sprintf "Must be greater than or equal to zero, was %f" i]

let valClosedInterval (a : decimal) (b : decimal) (i : decimal) : Validation =
  match inClosedInterval a b i with
  | true -> Valid
  | false -> Invalid [sprintf "Must be within interval [%f, %f] but was %f" a b i]

let (<?+>) (a : 'a -> Validation) (b : 'a -> Validation) : 'a -> Validation =
  fun (i : 'a) ->
    let r1 = a i
    let r2 = b i

    match r1, r2 with
    | Valid,      Valid       -> Valid
    | Invalid e,  Valid       -> Invalid e
    | Valid,      Invalid e   -> Invalid e
    | Invalid e1, Invalid e2  -> Invalid (e1 @ e2)


// +------------------------------------------------------+
// |                                                      |
// |  # Input Management                                  |
// |                                                      |
// +------------------------------------------------------+

///
/// parseCommand p a
/// p: position in the args array (0..n)
/// a: the args
/// 
let parseArg (p: int) (a: string[]) =
  match Array.length a with
  | l when l > p && p >= 0 -> Some(a.[p])
  | _ -> None 