module Dto

open FSharp.Json
open CommonLibrary
open Dike

  module PortfolioDto =

    type AssetDto = {
      [<JsonField("name")>]
      Name: string option
      [<JsonField("account")>]
      Account: string option
      [<JsonField("value")>]
      Value: decimal option
      [<JsonField("targetAllocation")>]
      TargetAllocation: decimal option
    }

    type T = {
      [<JsonField("availableCash")>]
      AvailableCash: decimal option
      [<JsonField("assets")>]
      Assets: AssetDto list option
    }

    let deserializeJson json =
      try Json.deserialize<T> json |> Ok with | e -> [e.Message] |> Error

    let toDomain (dto : T) =

      let toDisposableCash (dto : decimal option) : Result<Cash.T, string list> =
        mandatory Cash.build "Field AvailableCash must be set" dto

      let toAssets (dto : AssetDto list option) =
        let toAsset (dto : AssetDto) : Result<Asset.T list, string list> =
          let name = mandatory Name.build "Field Asset.Name is mandatory" dto.Name
          let account = mandatory Account.build "Field Asset.Account is mandatory" dto.Account
          let value = mandatory FaceValue.build "Field Asset.Value is mandatory" dto.Value
          let targetAllocation = mandatory TargetAllocation.build "Field Asset.TargetAllocation is mandatory" dto.TargetAllocation
          Asset.tryBuild name account value targetAllocation
          |> Result.map toList
 
        match dto with
        | Some v ->
          v 
          |> List.map toAsset
          |> List.reduce append
        | None -> ["A list of assets is mandatory"] |> Error

      let disposableCash = toDisposableCash dto.AvailableCash
      let assets = toAssets dto.Assets

      Portfolio.tryBuild disposableCash assets
