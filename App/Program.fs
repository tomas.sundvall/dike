open CommonLibrary
open Dto
open Dike

let parseInput argv =
  let i =
    match (parseArg 1 argv) with
    | Some i -> i
    | None -> stdin.ReadToEnd ()
  i

let maxLength (s : string list) : int =
  let rec maxLength' v =
    match v with
    | head::tail ->
      let l1 = head |> String.length
      let l2 = maxLength' tail
      if l1 > l2 then l1 else l2
    | [] -> 0

  maxLength' s

let printSep w =
  let row = "-" |> String.replicate (w - 2)
  printfn "+%s+" row


//
// outputSuccess
//
let outputSuccess (v : Transaction list) (assetsValue : decimal) =
  let col1Width = maxLength (v |> List.map (fun a -> Name.value a.Asset))
  let col2Width = 5
  let col3Width = 10
  let col4Width = 10

  printSep (col1Width + col2Width + col3Width + col4Width + 13)
  printfn "| %-*s | %-*s | %-*s | %-*s |" col1Width "Asset" col2Width "Side" col3Width "Value" col4Width "Account"
  printSep (col1Width + col2Width + col3Width + col4Width + 13)
  for t in v do
    let asset = Name.value t.Asset
    let account = Account.value t.Account
    let side = Portfolio.sideValue t.Side
    let value = Cash.value t.Cash
    printfn "| %-*s | %-*s | %-*.2f | %-*s |" col1Width asset col2Width side col3Width value col4Width account
  printSep (col1Width + col2Width + col3Width + col4Width + 13)
  printfn "| %-*s | %-*.2f |" (col1Width) "Total current asset value" (col2Width + col3Width + col4Width + 6) assetsValue
  printSep (col1Width + col2Width + col3Width + col4Width + 13)


let outputFailure (e : string list) =
  printfn "# FAILED"
  for e' in e do
    eprintfn "- %s" e'

[<EntryPoint>]
let main argv =
  let portfolio = 
    argv 
    |> parseInput 
    |> PortfolioDto.deserializeJson
    |+> PortfolioDto.toDomain 
    // |*> Portfolio.getTransactions

  // Portfolio   

  match portfolio with
  | Ok v -> 
    let transactions = Portfolio.getTransactions v
    let assetsValue = Portfolio.getAssetsValue v
    outputSuccess transactions assetsValue
    0
  | Error e ->
    outputFailure e
    1