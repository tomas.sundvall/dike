module Dike

open CommonLibrary

// +------------------------------------------------------+
// |                                                      |
// |  # Types                                             |
// |                                                      |
// +------------------------------------------------------+

//
// Cash
//
module Cash =
  type T = Cash of decimal

  let build (c : decimal) : Result<T, string list> =
    match valNotNegative c with
    | Valid -> Ok (Cash c)
    | Invalid e -> Error e

  let value (Cash c) : decimal = c


//
// Name
//
module Name =
  type T = Name of string

  let valName (i : string) =
    match matchRegex @"^[a-zA-Z 0-9\,\+\-]*$" i with
    | true -> Valid
    | false -> Invalid ["A name can only contain a-z, A-Z, 0-9 and ,+- characters."]

  let build (s : string) : Result<T, string list> =
    let v =
      valMinLength 1  <?+> 
      valMaxLength 50 <?+>
      valName

    match v s with
    | Valid -> Ok (Name s)
    | Invalid e -> Error e

  let value (Name s) : string = s


//
// Account
//
module Account =
  type T = Account of string

  let valAccount (i : string) =
    match matchRegex @"^[a-zA-Z 0-9\,\+\-]*$" i with
    | true -> Valid
    | false -> Invalid ["An account can only contain a-z, A-Z, 0-9 and ,+- characters."]

  let build (s : string) : Result<T, string list> =
    let v =
      valMinLength 1  <?+> 
      valMaxLength 50 <?+>
      valAccount

    match v s with
    | Valid -> Ok (Account s)
    | Invalid e -> Error e

  let value (Account s) : string = s


//
// Amount
//
module FaceValue =
  type T = FaceValue of decimal

  let build (c : decimal) : Result<T, string list> =
    match valNotNegative c with
    | Valid -> Ok (FaceValue c)
    | Invalid e -> Error e

  let value (FaceValue c) : decimal = c


//
// TargetAllocation
//
module TargetAllocation =
  type T = TargetAllocation of decimal

  let build (c : decimal) : Result<T, string list> =
    match valClosedInterval 0m 1m c with
    | Valid -> Ok (TargetAllocation c)
    | Invalid e -> Error e

  let value (TargetAllocation c) : decimal = c

type Side = BUY | SELL

type Transaction = { Side: Side; Asset: Name.T; Account: Account.T; Cash: Cash.T }

//
// Asset
//
module Asset =
  type T = {
    Name: Name.T
    Account: Account.T
    FaceValue: FaceValue.T
    TargetAllocation: TargetAllocation.T
  }

  let fvProj (a : T) : decimal =
    FaceValue.value a.FaceValue

  let taProj (a : T) : decimal =
    TargetAllocation.value a.TargetAllocation

  let build n a fv ta = {
    Name = n
    Account = a
    FaceValue = fv
    TargetAllocation = ta
  }

  let tryBuild 
    (n : Result<Name.T, 'c list>)
    (a : Result<Account.T, 'c list>)
    (fv : Result<FaceValue.T, 'c list>)
    (ta : Result<TargetAllocation.T, 'c list>)
    : Result<T, 'c list> =

    build <!> n <*> a <*> fv <*> ta

  let getTransaction (pv : decimal) (a : T) : Transaction =
    let d = ((a |> taProj) * pv) - (a |> fvProj)
    let side = 
      match d >= 0m with
      | true -> BUY
      | false -> SELL
    
    let { Name = n } = a

    let c = d |> abs |> Cash.build
    match c with
    | Ok v -> { Side=side; Asset=n; Account=a.Account; Cash=v }
    | Error _ -> invalidOp "Unexpectedly failed to generate transaction"

//
// Portfolio
//
module Portfolio =
  type T = {
    AvailableCash: Cash.T
    Assets: Asset.T list
  }

  let build (c : Cash.T) (a : Asset.T list) : Result<T, string list> =
    let totTa = a |> List.sumBy Asset.taProj
    match totTa with
    | 1.0m  -> Ok { AvailableCash = c; Assets = a }
    | _     -> Error [sprintf "The sum of the target allocation for all assets must be exactly 1 but was %f" totTa]
    
  let tryBuild
    (c : Result<Cash.T, string list>)
    (a : Result<Asset.T list, string list>)
    : Result<T, string list> =

    match build <!> c <*> a with
    | Ok a -> a
    | Error e -> Error e

  let faceValue (p : T) : decimal =
    let { Assets = a; AvailableCash = c } = p
    (a |> List.sumBy Asset.fvProj) + (c |> Cash.value)    

  let getTransactions (p : T) : Transaction list =
    let { Assets = a } = p
    let fv = faceValue p

    a |> List.map (Asset.getTransaction fv)

  let getAssetsValue (p : T) : decimal = 
    let { Assets = a } = p
    a 
    |> List.map (fun x -> FaceValue.value x.FaceValue) 
    |> List.reduce (fun a e -> a + e)
    

  let sideValue (s : Side) : string =
    match s with
    | BUY -> "Buy"
    | SELL -> "Sell"
