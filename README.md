# dike

This software calculates necessary transactions to retrieve a desired allocation of assets.

## Publish
```
dotnet publish -c Release -o ./pub
```

## Run Tests

```
dotnet watch test --project App.Tests/App.Tests.fsproj
```

## Run With Sample Data
Sample 1
```json
{
  "availableCash": 100000,
  "assets": [
    {
      "name": "fund one",
      "value": 50000,
      "targetAllocation": 0.5
    },
    {
      "name": "fund two",
      "value": 50000,
      "targetAllocation": 0.4
    },
    {
      "name": "fund with long name",
      "value": 50000,
      "targetAllocation": 0.1
    }
  ]
}
```

```
cat SampleInput/sample1.json | dotnet run --project App/App.fsproj
```

## Publish
```
dotnet publish ./App/App.fsproj -c Release -r osx.11.0-x64
```

https://www.michaelcrump.net/part3-aspnetcore/
https://docs.microsoft.com/en-us/dotnet/core/rid-catalog
