module Tests.Dike

open Xunit
open FsUnit.Xunit
open CommonLibrary
open Dike
open Utils

[<Theory>]
[<InlineData(100400.2)>]
[<InlineData(0)>]
let ``Test valid inputs for AvailableCash`` (c : decimal) =
  let expected : Result<decimal, string list> = (Ok c)
  Cash.build c |> Result.map Cash.value |> should equal expected

[<Theory>]
[<InlineData(-100400.2, 1)>]
let ``Test invalid inputs for AvailableCash`` (c : decimal) (e : int) =
  Cash.build c |> errorCount |> should equal e

[<Theory>]
[<InlineData(100400.2)>]
[<InlineData(0)>]
let ``Test valid inputs for Amount`` (c : decimal) =
  let expected : Result<decimal, string list> = (Ok c)
  FaceValue.build c |> Result.map FaceValue.value |> should equal expected

[<Theory>]
[<InlineData(-100400.2, 1)>]
let ``Test invalid inputs for Amount`` (c : decimal) (e : int) =
  FaceValue.build c |> errorCount |> should equal e

[<Theory>]
[<InlineData("This is a mutual fund")>]
let ``Test valid inputs for Name`` (n : string) =
  let expected : Result<string, List<string>> = (Ok n) 
  Name.build n |> Result.map Name.value |> should equal expected

[<Theory>]
[<InlineData("...", 1)>]
let ``Test invalid inputs for name`` (n : string) (e: int) =
  Name.build n |> errorCount |> should equal e

[<Theory>]
[<InlineData(0)>]
[<InlineData(0.2)>]
[<InlineData(0.125333)>]
[<InlineData(1)>]
let ``Test valid inputs for TargetAllocation`` (ta : decimal) =
  let expected : Result<decimal, List<string>> = (Ok ta)
  TargetAllocation.build ta |> Result.map TargetAllocation.value |> should equal expected

[<Theory>]
[<InlineData(1.001, 1)>]
[<InlineData(-0.0001, 1)>]
let ``Test invalid inputs for TargetAllocation`` (ta : decimal) (e : int) =
  TargetAllocation.build ta |> errorCount |> should equal e


[<Fact>]
let ``Test Asset.tryBuild with errors from all inputs`` () =
  Asset.tryBuild 
    (Error ["error one"; "error two"]) 
    (Error ["error three"; "error four"]) 
    (Error ["error five"; "error six"])
    (Error ["error five"; "error six"])
  |> errorCount
  |> should equal 8


[<Fact>]
let ``Test Asset.tryBuild with valid inputs`` () =
  Asset.tryBuild (Name.build "Ok name") (Account.build "Account name") (FaceValue.build 100m) (TargetAllocation.build 0.2m)
  |> resultOk
  |> should equal true


[<Fact>]
let ``Test Asset.tryBuild with invalid TargetAllocation and invalid Amount`` () =
  Asset.tryBuild (Name.build "Ok name") (Account.build "Account name") (FaceValue.build -100m) (TargetAllocation.build 1.5m)
  |> errorCount
  |> should equal 2


[<Fact>]
let ``Test Portfolio with negative results in input`` () =
  Portfolio.tryBuild
    (Error ["error one"])
    (Error ["error two"])
  |> errorCount
  |> should equal 2

[<Fact>]
let ``Test Portfolio with assets where target allocation does not sum to 1`` () =
  let assetOne = Asset.tryBuild (Name.build "a1") (Account.build "Account name") (FaceValue.build 100m) (TargetAllocation.build 0.5m)
  let assetTwo = Asset.tryBuild (Name.build "a2") (Account.build "Account name") (FaceValue.build 100m) (TargetAllocation.build 0.6m)
  let assets = assetOne <!*> assetTwo

  Portfolio.tryBuild (Cash.build 100m) assets
  |> errorCount
  |> should equal 1

[<Fact>]
let ``Test Portfolio with assets where target allocation sums to 1`` () =
  let assetOne = Asset.tryBuild (Name.build "a1") (Account.build "Account name") (FaceValue.build 100m) (TargetAllocation.build 0.5m)
  let assetTwo = Asset.tryBuild (Name.build "a2") (Account.build "Account name") (FaceValue.build 100m) (TargetAllocation.build 0.5m)
  let assets = assetOne <!*> assetTwo

  Portfolio.tryBuild (Cash.build 100m) assets
  |> resultOk
  |> should equal true

[<Fact>]
let ``Test Portfolio.totalValue`` () =
  let assetOne = Asset.tryBuild (Name.build "a1") (Account.build "Account name") (FaceValue.build 10000m) (TargetAllocation.build 0.5m)
  let assetTwo = Asset.tryBuild (Name.build "a2") (Account.build "Account name") (FaceValue.build 20000m) (TargetAllocation.build 0.4m)
  let assetThree = Asset.tryBuild (Name.build "a3") (Account.build "Account name") (FaceValue.build 0.5m) (TargetAllocation.build 0.1m)
  let assets = assetOne <!*> assetTwo <**> assetThree

  let expected : Result<decimal, string list> = (Ok 55000.5m)

  Portfolio.tryBuild (Cash.build 25000m) assets
  |> Result.map Portfolio.faceValue
  |> should equal expected

[<Fact>]
let ``Test Asset.getTransaction`` () =
  let expected : Result<(string*Side*decimal), string list> = (Ok ("a1", BUY, 10000m))

  Asset.tryBuild (Name.build "a1") (Account.build "Account name") (FaceValue.build 10000m) (TargetAllocation.build 0.1m)
  |> Result.map (Asset.getTransaction 200000m)
  |> Result.map (fun t -> (Name.value t.Asset, t.Side, Cash.value t.Cash))
  |> should equal expected

[<Fact>]
let ``Test Portfolio.getTransactions for an empty portfolio`` () =
  let getT (n : string) (t : Transaction list) =
    t |> List.find (fun x -> (Name.value x.Asset) = n)

  let mapT (t : Transaction) : (Side*decimal) =
    let { Side = s; Cash = c } = t
    (s, Cash.value c)

  let assetOne = Asset.tryBuild (Name.build "a1") (Account.build "Account name") (FaceValue.build 0m) (TargetAllocation.build 0.5m)
  let assetTwo = Asset.tryBuild (Name.build "a2") (Account.build "Account name") (FaceValue.build 0m) (TargetAllocation.build 0.4m)
  let assetThree = Asset.tryBuild (Name.build "a3") (Account.build "Account name") (FaceValue.build 0m) (TargetAllocation.build 0.1m)
  let assets = assetOne <!*> assetTwo <**> assetThree

  let t = 
    Portfolio.tryBuild (Cash.build 100000m) assets
    |> Result.map Portfolio.getTransactions

  let expectedA1 : Result<(Side*decimal), string list> = (BUY, 50000m) |> Ok
  let expectedA2 : Result<(Side*decimal), string list> = (BUY, 40000m) |> Ok
  let expectedA3 : Result<(Side*decimal), string list> = (BUY, 10000m) |> Ok

  t |> Result.map (getT "a1") |> Result.map mapT |> should equal expectedA1
  t |> Result.map (getT "a2") |> Result.map mapT |> should equal expectedA2
  t |> Result.map (getT "a3") |> Result.map mapT |> should equal expectedA3

[<Fact>]
let ``Test Portfolio.getTransactions`` () =
  let getT (n : string) (t : Transaction list) =
    t |> List.find (fun x -> (Name.value x.Asset) = n)

  let mapT (t : Transaction) : (Side*decimal) =
    let { Side = s; Cash = c } = t
    (s, Cash.value c)

  let assetOne = Asset.tryBuild (Name.build "a1") (Account.build "Account name") (FaceValue.build 100000m) (TargetAllocation.build 0.5m)
  let assetTwo = Asset.tryBuild (Name.build "a2") (Account.build "Account name") (FaceValue.build 100000m) (TargetAllocation.build 0.4m)
  let assetThree = Asset.tryBuild (Name.build "a3") (Account.build "Account name") (FaceValue.build 100000m) (TargetAllocation.build 0.1m)
  let assets = assetOne <!*> assetTwo <**> assetThree

  let t = 
    Portfolio.tryBuild (Cash.build 100000m) assets
    |> Result.map Portfolio.getTransactions

  let expectedA1 : Result<(Side*decimal), string list> = (BUY, 100000m) |> Ok
  let expectedA2 : Result<(Side*decimal), string list> = (BUY, 60000m) |> Ok
  let expectedA3 : Result<(Side*decimal), string list> = (SELL, 60000m) |> Ok

  t |> Result.map (getT "a1") |> Result.map mapT |> should equal expectedA1
  t |> Result.map (getT "a2") |> Result.map mapT |> should equal expectedA2
  t |> Result.map (getT "a3") |> Result.map mapT |> should equal expectedA3