module Utils

let errorCount (r : Result<'a, 'b list>) : int =
  match r with
  | Ok _ -> 0
  | Error e -> e |> List.length

let resultOk (r : Result<'a, 'b>) : bool =
  match r with
  | Ok _ -> true
  | Error _ -> false