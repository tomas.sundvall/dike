module App.Tests.Dike

open Xunit
open FsUnit.Xunit
open CommonLibrary
open Dto


[<Theory>]
[<InlineData("""{"availableCash": 1000, "assets":[{"name": "Tomas", "value": 100.25}]}""")>]
let ``Test json deserialization`` json =
  PortfolioDto.deserializeJson json |> printfn "JSON? %A"